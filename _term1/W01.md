---
title: MDEF Bootcamp
layout: "page"
order: 01
---

![image](http://gitlab.com/MDEF/landing/raw/master/assets/images/ca7bc18b25e9-bootcamp_S.jpg)

### Faculty
Tomas Diez  
Oscar Tomico  
Mariana Quintero

### Syllabus and Learning Objectives
The MDEF boot camp is landing and setup workshop that will introduce students to the main ambitions of the master program. The boot camp format will allow students to familiarize themselves with the physical spaces where the program will operate and experiment (classroom, lab, and neighborhood), as well as provide the initial tools to document and share their progress during their studies at IAAC. 

From Wikipedia: *“Boot camps can be governmental being part of the correctional and penal system of some countries. Modeled after military recruit training camps, these programs are based on shock incarceration grounded on military techniques. “*

Do not panic: IAAC is not a correctional facility! And we will only use the best of the boot camp format to facilitate the learning process and the adaptation of the students to the program and the available facilities.

### Total Duration
Classes: 12 hours (From Wednesday to Friday)  
Student work hours: 15 hours  
Total hours per student: 27 hours  

### Structure and Phases
* *Introduction - Wednesday*       
Introduction to the Master ambition, program, and structure
Introduction to Term 1

We want to know:  
Who are you, and why are you here?     
How do you see yourself in 10 years?     
  
* *The index of your book - Thursday morning*      
Tomas Diez presenting his work   
Mariana Quintero presenting her work  

Conversation about design paradigms  

*Poblenou Tour - Thursday afternoon*   
[Poblenou Urban District](http://www.poblenouurbandistrict.com/en/)   
[Leka Restaurant](https://restauranteleka.com/)   
[Indissoluble](https://www.indissoluble.com/)   
[TransfoLAB BCN](https://www.transfolabbcn.com/home)   
[BiciHub](http://bicihub.barcelona/)   
[ConnectHort](https://connecthort.wordpress.com/)   

* *What it means to design for emergent futures? - Friday*    
Introduction to the Design Studio    
Multiscalar design strategies  
Designing interventions  

### Output
Define your future you. Identify your possible areas of interest. Embed the Design for Emergent Futures culture.  
Assignment: Write a 1000 word essay about your expectations for the coming 9 months in Barcelona.  

### Bibliography
[Speculative Everything	Anthony Dunne and Fiona Raby](https://mitpress.mit.edu/books/speculative-everything)  
[Adversarial Design	Carl DiSalvo](https://mitpress.mit.edu/books/adversarial-design)  
[Massive Change	Bruce Mau, Jennifer Leonard and Institute without Boundaries](http://www.brucemaudesign.com/work/massive-change)  
[Design for the Real World: Human Ecology and Social Change	Victor Papanek](https://www.amazon.com/Design-%C2%ADReal-%C2%ADWorld-%C2%ADEcology-%C2%ADSocial/dp/0897331532)  
[Liquid Modernity	Zygmunt Bauman](https://www.amazon.com/Liquid-%C2%ADModernity-%C2%ADZygmunt-%C2%ADBauman/dp/0745624103)  
[Who Owns the Future?	Jason Lanier](https://www.amazon.es/gp/product/B008J2AEY8/ref=dp-%C2%ADkindle-%C2%ADredirect?ie=UTF8&btkr=1)  
[This Changes Everything	Naomi Klein](https://thischangeseverything.org/book/)  
[To Save Everything, Click Here: The Folly of Technological Solutionism	Evgeny Morozov](https://www.amazon.com/gp/product/1610393708?psc=1&redirect=true&ref_=oh_aui_detailpage_o01_s00)  
[Democratizing Innovation	Eric Von Hippel](https://www.amazon.com/gp/product/0262720477?psc=1&redirect=true&ref_=oh_aui_detailpage_o01_s01)  
[Cradle to Cradle: Remaking the Way We Make Things	Michael Braungart, William McDonough](http://www.amazon.com/gp/product/0865475873?psc=1&redirect=true&ref_=od_aui_detailpages00)  
[Macrowikinomics: New Solutions for a Connected Planet	Don Tapscott, Anthony D. Williams](http://www.amazon.com/gp/product/1591844282?psc=1&redirect=true&ref_=od_aui_detailpages00)  
[The Third Industrial Revolution: How Lateral Power Is Transforming Energy, the Economy, and the World	Jeremy Rifkin](http://www.amazon.com/gp/product/0230115217?psc=1&redirect=true&ref_=od_aui_detailpages00g)  

### Background Research Material
[Notion-Reading List](https://www.notion.so/mdef/417ebac5453f4f348d96687f40f5f848?v=31baffeef93c4cd0ac948982c458e734)

### Requirements for the Students
Personal computer, smart phone (install preferred mapping app), camera (optional), notebook. 

### Infrastructure Needs
Classroom 201. 

### TOMAS DIEZ
![image](https://gitlab.com/MDEF/landing/raw/master/assets/images/0302393192f2-Screenshot_2018_09_10_at_12.37.01_20180912191624503.jpg)

### Email Address
<tomasdiez@iaac.net>

### Personal Website
[Tomas Diez](http://tomasdiez.com)

### Twitter Account
@tomasdiez

### Facebook Profile
tomasdiez77

### Professional BIO
Tomas Diez is a Venezuelan Urbanist specialized in digital fabrication and its implications on the future cities and society. He is the co-founder of Fab Lab Barcelona, leads the Fab City Research Laboratory, and is a founding partner of the Fab City Global Initiative. He is the director of the Master in Design for Emergent Futures at the Institute for Advanced Architecture of Catalonia (IAAC) in Barcelona, where he is faculty in urban design and digital fabrication. Tomas is co-founder of other initiatives such as Smart Citizen (open source tools for citizen engagement), Fab City (locally productive, globally connected cities), Fablabs.io (the listing of fab labs in the world), and StudioP52 (art and design space in Barcelona).

### OSCAR TOMICO
![image](https://gitlab.com/MDEF/landing/raw/master/assets/images/6cf613d0f7fa-IMG_9099_Edit_S.jpg)

### Email Address
<otomico@elisava.net>

### Personal Website
[Oscar Tomico - Elisava](http://www.elisava.net/en/center/professorate/oscar-tomico-plasencia)

### Twitter Account
@otomico

### Professional BIO
Oscar Tomico holds an MSc degree in Industrial Engineering from Polytechnic University of Catalonia (Spain) and a PhD from the same institution, awarded in 2007 with Cum Laude. During his research into Innovation Processes in Product Design, he investigated subjective experience-gathering techniques based on constructivist psychology. After finishing his PhD he worked as a consultant for Telefonica R&D (Barcelona). Tomico joined Eindhoven University of Technology (TU/e) in 2007 as Assistant Professor. He has been a guest researcher and lecturer at AUT Creative technologies (New Zealand), at TaiwanTech (Taiwan), Swedish School of Textiles (Sweden), Institute of Advanced Architecture (Spain), University of Tsukuba, Aalto (Finland) to name a few. During his sabbatical in 2015 he worked as a consultant for the functional textiles department at EURECAT (Spain). He recently (2017) became the head of the Industrial Design Bachelor’s degree program at ELISAVA University School of Design and Engineering of Barcelona.

### MARIANA QUINTERO
![image](https://gitlab.com/MDEF/mdef-2019/raw/master/assets/images/about.jpg)

### Email Address
<mariana@fab.city>

### Personal Website
https://marianaquintero.io  

### Professional BIO
Multimedia developer, interaction designer & researcher, Mariana Quintero works and develops her practice at the intersection where digital fabrication technologies, digital literacy and information and computation ethics & aesthetics meet, contributing to projects that investigate the rise of the third digital revolution and how digital information and technologies translate, represent and mediate knowledge about the world. For the last three years she has been based in Barcelona contributing to research projects at IAAC \| Fab Lab Barcelona and developing digital literacy curricula.